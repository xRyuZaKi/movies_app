<?php

if (!function_exists('renderSection')) {
    function renderSection($section) {
        $CI = & get_instance();

        $CI->load->view($section);
    }
}

if (!function_exists('includeCSSResources')) {

    function includeCSSResources() {
        $cssResources = array(
            'assets/css/bootstrap',
            'assets/css/contactstyle',
            'assets/css/faqstyle',
            'assets/css/flexslider',
            'assets/css/font-awesome.min',
            'assets/css/jquery.slidey.min',
            'assets/css/medile',
            'assets/css/news',
            'assets/css/owl.carousel',
            'assets/css/popuo-box',
            'assets/css/single',
            'assets/css/style',
            'assets/css/movies-app'
        );

        foreach ($cssResources as $resource) {
            echo '<link rel="stylesheet" href="' . base_url() . $resource . '.css">';
        }
    }
}

if (!function_exists('includeJSResources')) {

    function includeJSResources() {

        $jsResources = array(
            "assets/js/jquery-2.1.4.min",
            "assets/js/bootstrap.min",
            "assets/js/easing",
            "assets/js/jquery.dotdotdot.min",
            "assets/js/jquery.flexslider",
            "assets/js/jquery.magnific-popup",
            "assets/js/jquery.slidey",
            "assets/js/move-top",
            "assets/js/owl.carousel",
            "assets/js/simplePlayer",
            "assets/js/bootstrap3-typeahead.min",
            "assets/js/api",
            "assets/js/service",
            "assets/js/javascript"
        );

        foreach ($jsResources as $resource) {
            echo '<script type="text/javascript" src="' . base_url() . $resource . '.js"></script>';
        }
    }
}

if (!function_exists('GetFromPostWithEncryptedPwd')) {
    function GetFromPostWithEncryptPwd($pwd, $pwd2 = null) {
        // How to sanitize each element??
        $post = $_POST;
        $obj = array();

        foreach ($post as $k => $v) {
            if ($pwd === $k || $pwd2 === $k) {
                $obj[$k] = md5($v);
            } else {
                $obj[$k] = $v;
            }
        }
        return $obj;
    }
}

if (!function_exists('GetFromSession')) {
    function GetFromSession($var) {
        $CI = & get_instance();

        return $CI->session->userdata($var);
    }
}

if (!function_exists('GetAllFromSession')) {
    function GetAllFromSession() {
        $CI = & get_instance();

        return $CI->session->all_userdata();
    }
}

if(!function_exists("getAllGenders")){
    function getAllGenders(){
        $CI = & get_instance();
        $CI->load->model("Main_Model");

        $model = new Main_Model();

        return $model->getAllGenders();;
    }
}

if (!function_exists('GetAllPost')) {
    function GetAllPost() {
        $post = $_POST;
        $obj = array();

        foreach ($post as $k => $v) {
            $obj[$k] = $v;
        }

        return (object) $obj;
    }
}

if (!function_exists("CreateSlug")){
    function CreateSlug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
    }
}

if (!function_exists("SetSession")) {
    function SetSession($key, $data) {
        $CI = & get_instance();

        $CI->session->set_flashdata($key, $data);
    }

}

if (!function_exists("GetSession")) {
    function GetSession($key) {
        $CI = & get_instance();

        return $CI->session->flashdata($key);
    }
}

if (!function_exists("Encode")){
    function Encode($data){
        $CI = & get_instance();

        $data = json_encode($data);
        $data = $CI->encrypt->encode($data);
        $data = \str_replace(array("+", "=", "/"), array(".", "-", "~"), $data);
        return $data;

        return $data;
    }
}

if (!function_exists("Decode")){
    function Decode($data, $assoc = FALSE){
        $CI = & get_instance();

        $data = \str_replace(array(".", "-", "~"), array("+", "=", "/"), $data);
        $data = $CI->encrypt->decode($data);
        $data = json_decode($data, $assoc);

        return $data;
    }
}

if (!function_exists('ModalAddDetailDownload')) {

    function ModalAddDetailDownload($title, $label, $controller, $mod, $placeholder) {
        // Capitalize First Letter
        $modInput = ucfirst(strtolower($mod));
        $formOpen = form_open($controller);
        $formClose = form_close();

        return <<<MODAL
		<div class="modal fade" id="modal-$mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">$title</h4>
                    </div>
                    $formOpen
                    <div class="modal-body">
                        <div class="input-group">
                            <span class="input-group-addon" id="sizing-addon3">$label</span>
                            <input type="text" name="txt$modInput" id="txt$modInput" class="form-control" placeholder="$placeholder" aria-describedby="sizing-addon3">
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" name="operation" value="save$modInput" class="btn btn-primary">Aceptar</button>
                    </div>
                    $formClose
                </div>
            </div>
        </div>
MODAL;
    }
}

if (!function_exists('ModalAddDetailDownloadServer')) {

    function ModalAddDetailDownloadServer() {

        $formOpen = form_open("Detail_Download/save");
        $formClose = form_close();

        return <<<MODAL
        <div class="modal fade" id="modal-server" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Servidores</h4>
                    </div>
                    $formOpen
                    <div class="modal-body">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon3">Servidor</span>
                        <input type="text" name="txtServer" id="txtServer" class="form-control" placeholder="[Servidor: Ejem. Mega, MediaFire...]" aria-describedby="sizing-addon3">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon3">Tipo</span>
                        <select name="cmbType" id="cmbType" class="form-control">
                            <option value="online">OnLine</option>
                            <option value="download">Descarga Directa</option>
                        </select>
                    </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" name="operation" value="saveServer" class="btn btn-primary">Aceptar</button>
                    </div>
                    $formClose
                </div>
            </div>
        </div>
MODAL;
    }
}