<?php

if (!function_exists('AppInitialize')) {
    function AppInitialize($userData){
        // Get CI reference
        $CI =& get_instance();

        // Create User Session
        $userData['isloggedin'] = true;
        $CI->session->set_userdata($userData);
        
        return $CI->session->all_userdata();
    }
}

if (!function_exists('AppShutDown')) {
    function AppShutDown(){
        $CI =& get_instance();
        $CI->session->sess_destroy();

        redirect(base_url() . 'main');
    }
}