var api = {
    post: function (url, data, done) {
        $.ajax({
            url: url,
            dataType: "json",
            method: "POST",
            data: data,
        }).done(done);
    }
};