$(document).ready(function() {
	$("#owl-demo").owlCarousel({
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		items: 5,
		itemsDesktop: [640, 4],
		itemsDesktopSmall: [414, 3]
	});

	$("#slidey").slidey({
		interval: 8000,
		listCount: 5,
		autoplay: false,
		showList: true
	});
	$(".slidey-list-description").dotdotdot();

	$(".search").typeahead({
		source: function(query, process) {
			console.log(query);
			var data = {
				query: query
			};

			var url = baseUrl() + "search/multi_search";
			service.search.get(url, data, function(response) {
				var resultList = response.map(function(item) {
					var aItem = {
						backdrop_path: item.backdrop_path,
						original_title: item.original_title,
						dataEncode: item.dataEncode
					};
					return JSON.stringify(aItem);
				});

				return process(resultList);
			});
		},

		matcher: function(obj) {
			return true;
		},

		highlighter: function(obj) {
			var item = JSON.parse(obj);
			var image =
				'<img style="width: 50px; heigth: 50px" src="data:image/jpg;base64,' +
				item.backdrop_path +
				'">';

			return image + item.original_title;
		},

		updater: function(obj) {
			var item = JSON.parse(obj);
			//return item.name;
			//window.location.href = baseUrl() + "mostrar/pelicula-tv-show-" + slugify(item.original_title) + "/" + item.dataEncode;
			window.location.href =
				baseUrl() +
				"manage/save/" +
				item.dataEncode +
				"/" +
				slugify(item.original_title);
		}
	});

	$(".save-link").click(function(e) {
		/*var id = jQuery(this).attr("data-id");
        alert("Value: " + id);*/
        var value = jQuery(this).attr("data-name");
        $("#labelAddLinks").html("Agregar Enlaces: " + value);
	});

	$("#cmbSeason").change(function(){
		var idSeason = $("#cmbSeason").val();
		var idTv = $("#btnConfirm").val();

		var data = {
			"idSeason": idSeason,
			"idTv": idTv
		};

		var url = baseUrl() + "show/getChapters";
		service.tvShow.chapters(url, data, function (response){
			console.log(response);
			var option =  "";
			for(var key in response){
				option = option + "<option value='" + key + "'>" + response[key] +"</option>"
			}

			$("#cmbChapter").html(option);
		});
	});
});

function slugify(text) {
	return text
		.toString()
		.toLowerCase()
		.replace(/\s+/g, "-") // Replace spaces with -
		.replace(/[^\w\-]+/g, "") // Remove all non-word chars
		.replace(/\-\-+/g, "-") // Replace multiple - with single -
		.replace(/^-+/, "") // Trim - from start of text
		.replace(/-+$/, ""); // Trim - from end of text
}
