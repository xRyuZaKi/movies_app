<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MZ_Controller {

	public function __construct()
	{
		parent::__construct();
		self::LoadModel("Manage_Model");
	}

	public function index()
	{
		$movies = self::$model->getLinks();
		$settings = [
			"section" => "main/mainView",
			"movies" => $movies
		];

		self::MakeView($settings);
	}
}
