<?php

class Manage extends MZ_Controller{

    public function __construct(){
        parent::__construct();

        self::UserAuth();
        self::LoadLibrary("guzzle");
        self::LoadModel('Manage_Model');
    }

    public function index(){
          
    }

    public function show($name, $encodeInfoShow =  null){
        if ($encodeInfoShow != NULL){
            SetSession("encodeInfoShow", $encodeInfoShow);
            redirect(base_url("mostrar/pelicula-tv-show-$name.html"));
        }

        $encodeInfoShow = GetSession("encodeInfoShow");
        $decodeInfoShow = Decode($encodeInfoShow, TRUE);

        $mediaType = $decodeInfoShow["mediaType"];
        $idTMDB = $decodeInfoShow["idTMDB"];

        if($mediaType == "movie"){
            $urlRequest = self::$urlAPI . "movie/$idTMDB?api_key=" . self::$secretKey . "&language=es-ES";
        }else if($mediaType == "tv"){
            $urlRequest = self::$urlAPI . "tv/$idTMDB?api_key=" . self::$secretKey ."&language=es-ES";
        }

        SetSession("encodeInfoShow", $encodeInfoShow);

        $client  = new GuzzleHttp\Client();
        $resultRequest = $client->request("GET", $urlRequest);
        $result = json_decode($resultRequest->getBody(), TRUE);

        if(!isset($result["original_title"]) && !isset($result["title"])){
            $title = $result["original_name"];
        }else{
            $title = $result["title"];
        }

        if(!isset($result["release_date"])){
            $releaseDate = $result["first_air_date"];
        }else{
            $releaseDate = $result["release_date"];
        }

        $dataInformation = [
            "poster_path" => $result["poster_path"],
            "backdrop_path" => $result["backdrop_path"],
            "original_title" => $title,
            "release_date" => $releaseDate,
            "overview" => $result["overview"],
            "genres" => $result["genres"]
        ];
        $settings = [
            "section" => "manage/showView",
            "dataInformation" => $dataInformation
        ];

        self::MakeView($settings);
    }

    public function save($encodeInfoShow, $slug_url){
        //$encodeInfoShow = GetSession("encodeInfoShow");
        $decodeInfoShow = Decode($encodeInfoShow, TRUE);

        $mediaType = $decodeInfoShow["mediaType"];
        $idTMDB = $decodeInfoShow["idTMDB"];

        if($mediaType == "movie"){
            $urlRequest = self::$urlAPI . "movie/$idTMDB?api_key=" . self::$secretKey . "&language=es-ES";
        }else if($mediaType == "tv"){
            $urlRequest = self::$urlAPI . "tv/$idTMDB?api_key=" . self::$secretKey ."&language=es-ES";
        }

        SetSession("encodeInfoShow", $encodeInfoShow);

        $client  = new GuzzleHttp\Client();
        $resultRequest = $client->request("GET", $urlRequest);
        $result = json_decode($resultRequest->getBody(), TRUE);

        try{
            $backdropPath = realpath('') . "/assets/images/poster/backdrop_path_" . $mediaType . "_" . $result["id"] . ".jpg";
            $posterPath = realpath('') . "/assets/images/poster/poster_path_" . $mediaType . "_" . $result["id"] . ".jpg";

            if(!isset($result["backdrop_path"])){
                $imgSourceBackdropPath = "https://dummyimage.com/600x400/000/fff";
            }else{
                $imgSourceBackdropPath = "http://image.tmdb.org/t/p/w500" . $result["backdrop_path"];
            }

            if(!isset($result["poster_path"])){
                $imgSourcePosterPath = "https://dummyimage.com/600x400/000/fff";
            }else{
                $imgSourcePosterPath = "http://image.tmdb.org/t/p/w500" . $result["poster_path"];
            }
            copy($imgSourceBackdropPath, $backdropPath);
            copy($imgSourcePosterPath, $posterPath);

            $dataInsert = [
                "backdrop_path" =>  "backdrop_path_" . $mediaType . "_" . $result["id"] . ".jpg",
                "poster_path" =>  "poster_path_" . $mediaType . "_" . $result["id"] . ".jpg",
                "id_tmdb" => $result["id"],
                "original_title" => (isset($result["original_title"]))? $result["original_title"] : NULL,
                "original_name" => (isset($result["original_name"]))? $result["original_name"] : NULL,
                "title" => (isset($result["title"]))? $result["title"] : NULL,
                "overview" => $result["overview"],
                "release_date" => (isset($result["release_date"]))? $result["release_date"] : NULL,
                "first_air_date" => (isset($result["first_air_date"]))? $result["first_air_date"] : NULL,
                "vote_average" => $result["vote_average"],
                "runtime" => (isset($result["runtime"])) ? $result["runtime"] : NULL,
                "type" => $mediaType,
                "slug_url" => $slug_url
            ];

            $genres = $result["genres"];
            $genresInsert = [];
            foreach($genres as $value){
                $genresInsert[] = $value["id"];
            }

            $resultSeasonChapters = [];
            if($mediaType == "tv"){
                $resultSeasonChapters = $this->getSeasonsChapters($result["id"]);
            }
            self::$model->save($dataInsert, $genresInsert, $this->getActors($result["id"], $mediaType), $resultSeasonChapters);
            redirect(base_url("panel.html"));
        }catch(Exception $e){

        }
    }

    public function saveLinks(){
        self::$model->saveLinkDownloadOnline();

        $dataPost = (array)GetAllPost();
        $idMovieTv = $dataPost["btnConfirm"];

        $movieTv = self::$model->getMovieTv($idMovieTv);
        $slugUrl = $movieTv["slug_url"];

        redirect(base_url("mostrar/pelicula-tv-show-" . $slugUrl . ".html"));
    }

    private function getActors($idTmdb, $type){
        if($type == "movie"){
            $urlRequest = self::$urlAPI . "movie/$idTmdb/credits?api_key=" . self::$secretKey . "&language=es-ES";
        }else if($type == "tv"){
            $urlRequest = self::$urlAPI . "tv/$idTmdb/credits?api_key=" . self::$secretKey . "&language=es-ES";
        }

        $client  = new GuzzleHttp\Client();
        $resultRequest = $client->request("GET", $urlRequest);
        $result = json_decode($resultRequest->getBody(), TRUE);

        $resultActors = [];
        foreach($result["cast"] as $item){
            if(!is_null($item["profile_path"]) || !empty($item["profile_path"])){
                $resultActors[] = [
                    "name" => $item["name"]
                    , "character" => $item["character"]
                ];
            }
        }

        return $resultActors;
    }

    private function getSeasonsChapters($idTmdb){
        $urlRequestSeasons = self::$urlAPI . "tv/$idTmdb?api_key=" . self::$secretKey ."&language=es-ES";

        $client  = new GuzzleHttp\Client();
        $resultRequestSeasons = $client->request("GET", $urlRequestSeasons);
        $resultSeasons = json_decode($resultRequestSeasons->getBody(), TRUE);

        $seasons = $resultSeasons["seasons"];

        foreach($seasons as $item){
            $seasonNumber = $item["season_number"];

            $urlRequestChapters = self::$urlAPI . "tv/$idTmdb/season/$seasonNumber?api_key=". self::$secretKey ."&language=es-ES";

            $resultRequestChapters = $client->request("GET", $urlRequestChapters);
            $resultChapters = json_decode($resultRequestChapters->getBody(), true);
            $episodes = $resultChapters["episodes"];

            $dataEpisodes = [];
            foreach($episodes as $value){
                $dataEpisodes[] = [
                    "episode_number" => $value["episode_number"]
                    , "name" => $value["name"]
                    , "overview" => $value["overview"]
                    , "air_date" => $value["air_date"]
                ];
            }

            $dataSeasons[] = [
                "season_number" => $seasonNumber
                , "name" => $item["name"]
                , "overview" => $item["overview"]
                , "air_date" => $item["air_date"]
                , "episodes" => $dataEpisodes
            ];
        }
        
        return $dataSeasons;
    }

}
