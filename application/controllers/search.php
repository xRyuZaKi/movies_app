<?php

class Search extends MZ_Controller{

    public function __construct(){
        parent::__construct();

        self::UserAuth();
        self::LoadLibrary("guzzle");
    }

    public function index(){
          
    }
    
    public function multi_search(){
        if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
        }
        
        try{
            $dataPost = GetAllPost();
			$query = $dataPost->query;

			$urlRequest = self::$urlAPI . "search/multi?api_key=" . self::$secretKey . "&language=es-ES&query=$query&page=1&include_adult=false;";
            $client = new GuzzleHttp\Client();
			$request = $client->request("GET", $urlRequest);

			$result = json_decode($request->getBody(), TRUE);
			$result = $result["results"];
            $data = array();
            
			foreach($result as $value){
                if(!isset($value["original_title"])){
                    $title = (!isset($value["original_name"])) ? (!isset($value["original_title"])? $value["name"] : $value["original_title"]) : $value["original_name"] ;
                }else{
                    $title = $value["title"];
                }

                if(!isset($value["backdrop_path"])){
                    $imgSource = "https://dummyimage.com/600x400/000/fff";
                }else{
                    $imgSource = "http://image.tmdb.org/t/p/w500" . $value["backdrop_path"];
                }

                $imgBinary = file_get_contents($imgSource);
                $imgString = base64_encode($imgBinary);

                $encode = [
                    "idTMDB" => $value["id"], 
                    "mediaType" => $value["media_type"]
                ];
                $data[] = [
                    "backdrop_path" => $imgString
                    , "original_title" => $title
                    , "dataEncode" => Encode($encode)
                ];
			}

            echo json_encode($data);
        }catch(Exception $e){

        }
    }
}