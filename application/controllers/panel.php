<?php
class Panel extends MZ_Controller{

    public function __construct(){
        parent::__construct();

        self::UserAuth();
        self::LoadModel("Manage_Model");
    }

    public function index(){
        $this->mainView();
    }

    private function mainView(){
        $result = self::$model->getAllMovieTvShow();
		foreach($result as &$value){
            if(!isset($value["original_title"]) && !isset($value["title"])){
                $title = $value["original_name"];
            }else{
                $title = $value["title"];
            }

            $value["title"] = $title;

            if(isset($value["release_date"])){
                $release_date = $value["release_date"];
            }else{
                $release_date = $value["first_air_date"];
            }

            $value["release_date"] = $release_date;
		}

        $settings = [
            "title" => "Panel Uploader",
            "section" => "panel/mainView",
            "result" => $result
        ];

        self::MakeView($settings);
    }

}