<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MZ_Controller{

    public function __construct(){
        parent::__construct();

        self::LoadModel("User_Model");
    }

    public function index(){

    }

    public function login(){
        $credentials = GetFromPostWithEncryptPwd('password');

        $userData = self::$model->_validateCredentials($credentials);

        if($userData){
            AppInitialize($userData);
            redirect(base_url());
        }
    }

    public function logout(){
        AppShutDown();

        redirect(base_url());
    }

    public function account(){
        $settings = [
            "title" => "Mi Cuenta",
            "section" => "profile/mainView"
        ];

        self::MakeView($settings);
    }
}