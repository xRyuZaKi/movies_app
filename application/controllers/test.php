<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MZ_Controller {

	public function index()
	{
		p(Encode(550));
		p(Decode(Encode(550)));
		exit;
		/*static::LoadLibrary("guzzle");

		$url = "https://api.themoviedb.org/3/movie/popular?api_key=28ff816145fbe5c7a4311fb8df84342b";
		$client = new GuzzleHttp\Client();
		$response = $client->request("GET", $url);
		
		p(json_decode($response->getBody()), TRUE);*/

		$config['uri_segment'] = 'start';
        $config['total_rows'] = 19800;
        $config['per_page'] = 20;
        $config['num_links'] = 10;
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['next_link'] = 'Siguiente';
        $config['prev_link'] = 'Anterior';
		$config['anchor_class'] = 'class = "btn btn-default"';
		//$this->load->library('pag');
		static::LoadLibrary("pag");
		$this->pag->initialize($config);
		
		$pagination = $this->pag->create_links();

		$settings = [
			"section" => "welcome_message"
			, "pagination" => $pagination
		];
		static::MakeView($settings);

	}

	protected function encode_filtrado($value) {//Encriptacion del Dato
        $value = json_encode($value);
        $value = $this->encrypt->encode($value);
        $value = \str_replace(array("+", "=", "/"), array(".", "-", "~"), $value);
        return $value;
    }

    protected function decode_filtrado($value, $assoc = false) {//Desencriptacion
        $value = \str_replace(array(".", "-", "~"), array("+", "=", "/"), $value);
        $value = $this->encrypt->decode($value);
        $value = json_decode($value, $assoc);
        return $value;
    }
}
