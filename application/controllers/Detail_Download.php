<?php
class Detail_Download extends MZ_Controller{

    public function __construct(){
        parent::__construct();

        self::UserAuth();
        self::LoadModel("Manage_Model");
    }

    public function index(){
        $settings = [
            "section" => "detaildownload/mainView"
            , "servers" => self::$model->getDetailDownload("server")
            , "languages" => self::$model->getDetailDownload("language")
            , "subtitles" => self::$model->getDetailDownload("subtitle")
            , "qualities" => self::$model->getDetailDownload("quality")
        ];

        self::MakeView($settings);
    }

    public function save(){
        $operation = $this->input->post("operation");
        $dataPost = (array)GetAllPost();
        p($operation, $dataPost);
        switch($operation){
            case "saveLanguage":
                $dataInsert=[
                    "description" => $dataPost["txtLanguage"]
                ];
                self::$model->saveDetailDownload("language", $dataInsert);
            break;

            case "saveQuality":
                $dataInsert = [
                    "description" => $dataPost["txtQuality"]
                ];
                self::$model->saveDetailDownload("quality", $dataInsert);
            break;

            case "saveSubtitle":
                $dataInsert = [
                    "description" => $dataPost["txtSubtitle"]
                ];
                self::$model->saveDetailDownload("subtitle", $dataInsert);
            break;

            case "saveServer":
                $dataInsert = [
                    "description" => $dataPost["txtServer"]
                    , "type" => $dataPost["cmbType"]
                ];
                self::$model->saveDetailDownload("server", $dataInsert);
            break;
        }

        redirect(base_url("Detail_Download.html"));
    }
}