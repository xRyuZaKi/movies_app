<?php
class Movies extends MZ_Controller{

    public function __construct(){
        parent::__construct();
        self::LoadModel("Manage_Model");
    }

    public function index(){
        
        $movies = self::$model->getLinks();
        
        $settings  = [
            "section" => "movies/mainView",
            "movies" => $movies
        ];
        
        self::MakeView($settings);
    }

}