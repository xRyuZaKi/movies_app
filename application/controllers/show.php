<?php
class Show extends MZ_Controller{

    public function __construct(){
        parent::__construct();

        self::UserAuth();
        self::LoadModel("Manage_Model");
        self::LoadLibrary("guzzle");
    }

    public function index(){}
    
    public function detail($slugUrl){
        $dataInformation = self::$model->isValidSlugUrl($slugUrl);
        
        if(count($dataInformation) > 0){

            $imgSource = realpath('') . "/assets/images/poster/" . $dataInformation["poster_path"];
            $imgBinary = file_get_contents($imgSource);
            $imgString = base64_encode($imgBinary);

            $genresMovieTv = self::$model->genresMovieTv($dataInformation["id"]);
            $dataInformation["poster_path"] = $imgString;

            $cmbQuality = ["default" => "Calidad"];
            $cmbLanguage = ["default" => "Idioma"];
            $cmbSubtitle = ["default" => "SubTitulos"];
            $cmbServer = ["default" => "Servidores"];

            $qualities = self::LoadModel("Manage_Model")->getDetailDownload("quality");
            $languages = self::LoadModel("Manage_Model")->getDetailDownload("language");
            $subtitles = self::LoadModel("Manage_Model")->getDetailDownload("subtitle");
            $servers = self::LoadModel("Manage_Model")->getDetailDownload("server");

            foreach($qualities as $item){
                $cmbQuality[$item["id"]] = $item["description"];
            }

            foreach($languages as $item){
                $cmbLanguage[$item["id"]] = $item["description"];
            }

            foreach($subtitles as $item){
                $cmbSubtitle[$item["id"]] = $item["description"];
            }

            $strCmbServer = '<select name="cmbServer[]" class="form-control"> <option value="default">Servidores</option>';
            foreach($servers as $item){
                $cmbServer[$item["id"]] = $item["description"] . "(" . $item["type"] . ")";
                $id = $item["id"];
                $strCmbServer = $strCmbServer . '<option value="'.$id.'">' . $item["description"] . "(" . $item["type"] . ")" . '</option>';
            }

            $strCmbServer = $strCmbServer . "</select>";

            $settings = [
                "title" => "Detalle",
                "section" => "panel/showView",
                "dataInformation" => $dataInformation,
                "genres" => $genresMovieTv,
                "cmbServer" => $strCmbServer,
                "cmbLanguage" => $cmbLanguage,
                "cmbSubtitle" => $cmbSubtitle,
                "cmbQuality" => $cmbQuality,
                "actors" => self::$model->actorsMovieTv($dataInformation["id"]),
                "links" => ($dataInformation["type"] == "tv") ? self::$model->getLinksTVShow($dataInformation["id"]) : self::$model->getLinks($dataInformation["id"])
            ];

            if($dataInformation["type"] == "tv"){
                $seasons = self::$model->getSeasons($dataInformation["id"]);
                $cmbSeasons = [
                    "default" => 'Temporada'
                ];
                foreach($seasons as $season){
                    $cmbSeasons[$season["season_number"]] = $season["name"];
                }

                $settings["cmbSeasons"] = $cmbSeasons;
            }

            self::MakeView($settings);
        }else{

        }
    }

    public function getChapters(){
        $idSeason = $this->input->post("idSeason");
        $idTv = $this->input->post("idTv");

        $chapters = self::$model->getChapters($idTv, $idSeason);
        $cmbChapters = [];

        foreach($chapters as $chapter){
            $cmbChapters[$chapter["id"]] = "Cap. " . $chapter["episode_number"] . ": " . $chapter["name"];
        }

        echo json_encode($cmbChapters);
    }

}