<div class="single-page-agile-main">
    <div class="container">
        <section>
            <aside class="text-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#modalUploader">
                    Agregar Ficha
                </button>
                <a href="<?php echo base_url("Detail_Download.html") ?>"
                    class="btn btn-primary"
                >
                    <i class="fa fa-info"></i> Gestionar Detalles Descarga
                </a>
            </aside>
            <hr>
            <section>
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Pelicula / Tv Show</th>
                            <th>Fecha Emision</th>
                            <th>Categoria</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $i = 0;
                        foreach($result as $item){ ?>
                            <tr>
                                <td><?php echo ++$i; ?></td>
                                <td><?php echo $item["title"]; ?></td>
                                <td><?php echo $item["release_date"]; ?></td>
                                <td><?php echo ($item["type"] == "movie")? "Pelicula" : "Serie" ; ?></td>
                                <td>
                                    <aside>
                                        <button type="button" class="btn btn-primary btn-xs save-link"
                                            data-toggle="modal"
                                            data-target="#modalLinks"
                                            data-id = "<?php echo $item["id"] ?>"
                                            data-name = "<?php echo $item["title"] ?>"
                                        >
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <a href="<?php echo base_url("mostrar/pelicula-tv-show-" . $item["slug_url"]); ?>.html" class="btn btn-default btn-xs">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </aside>
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </section>
        </section>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalUploader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Ficha</h4>
            </div>
            <div class="modal-body">
                <input class="form-control search" type="text" id="search" name="Search" placeholder="Search" required="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalLinks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelAddLinks"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-2">
                        <select class="form-control" name="cmbSeason" id="cmbSeason">
                            <option value="default">Temporada</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="cmbChapter" id="cmbChapter">
                            <option value="default">Capitulo</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="cmbQuality" id="cmbQuality">
                            <option value="default">Calidad</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="cmbLanguage" id="cmbLanguage">
                            <option value="default">Idioma</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="cmbSubtitle" id="cmbSubtitle">
                            <option value="default">Subtitulos</option>
                        </select>
                    </div>
                </div>
                <hr>
                
                <button type="button" class="btn btn-default btn-xs add" id="add">
                    <i class="fa fa-plus"></i>
                </button>
                <br><br>

                <?php echo form_open(base_url("manage/saveLinks")); ?>
                <div id="buildyourform"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                
                <button type="submit" class="btn btn-primary">Confirmar</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>