<div class="single-page-agile-main">
    <div class="container">
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-2">
                    <div class="row">
                        <div class="col-sm-12">
                            <img class="center-block img-responsive" src="data:image/jpg;base64,<?php echo $dataInformation["poster_path"] ?>"/>
                            <br>
                            <aside class="text-center">
                                <button style="width: 100%" class="btn btn-danger">
                                    Ver Trailer
                                </button>
                            </aside>
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-10">
                    <p class="text-justify"><h1><?php echo $dataInformation["title"] ?></h1></p>
                    <div class="row">
                        <div class="col-sm-2">
                            <i class="fa fa-star"></i> <?php echo $dataInformation["vote_average"] ?>/10
                        </div>
                        <div class="col-sm-2">
                            <i class="fa fa-heart"></i>
                            <i class="fa fa-clock-o"></i>
                            <i class="fa fa-plus"></i>
                            <i class="fa fa-eye"></i>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="text-justify">
                                <?php echo $dataInformation["overview"] ?>
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            Duracion: <?php echo ($dataInformation["type"] == "tv") ? NULL : $dataInformation["runtime"] . "min" ; ?>
                        </div>
                        <div class="col-sm-6">
                            Genero:
                            <small>
                                <?php 
                                foreach($genres as $value){
                                    echo '<span class="badge">' . $value["name"] . '</span>';
                                }
                                ?>
                            </small>
                        </div>
                    </div>
                    <hr>
                    <h3>Actores</h3>
                    <?php
                        /*$columns = 6;
                        $col = 12/$columns;
                        $rows = ceil(count($actors) / $columns);
                        $j = 1;
                        for($i = 1; $i <= $rows; $i++ ){
                            echo '<div class="row">';
                            foreach($actors as $key => $item){
                                if($j <= $columns){
                                    echo '<div class="col-sm-'.$col.'">';
                                    echo "<p class='text-center'>" . $item["name"] . " - " . $item["character"] . "</p>";
                                    unset($actors[$key]);
                                    echo '</div>';
                                }else{
                                    break;
                                }
                                $j++;
                            }
                            $j = 1;
                            echo '</div>';
                            echo "<hr>";
                        }*/
                    ?>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-8">
                        <aside>
                            <button class="btn btn-primary">Descarga</button>
                            <button class="btn btn-primary">Online</button>
                        </aside>
                    </div>
                    <div class="col-sm-2">
                        <aside>
                            <button type="button" class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#modalLinks"
                            >
                                <i class="fa fa-link"></i> Agregar Enlaces
                            </button>
                        </aside>
                    </div>
                </div>
                <section>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                            <?php if($dataInformation["type"] == "tv"){
                                echo "<th>Temporada</th>";
                                echo "<th>Capitulo</th>";
                            } ?>
                                <th>Server</th>
                                <th>Calidad</th>
                                <th>Idioma</th>
                                <th>SubTitulo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($links as $link){?>
                            <tr>
                                <?php if($dataInformation["type"] == "tv"){
                                    echo "<td> Temp. " . $link["season_number"] . "</td>";
                                    echo "<td> Cap. " . $link["episode_number"] . ": " . $link["name"] . "</td>";
                                } ?>
                                <td><?php echo $link["server"] ?></td>
                                <td><?php echo $link["quality"] ?></td>
                                <td><?php echo $link["language"] ?></td>
                                <td><?php echo $link["subtitle"] ?></td>
                                <td><?php echo $link["linkDownload"] ?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </section>
            </div>
            <div class="col-sm-2">
                <div class="row">
                    <div class="col-sm-12">
                        <ul>
                            <li>Poster 1</li>
                            <li>Poster 2</li>
                            <li>Poster 3</li>
                            <li>Poster 4</li>
                            <li>Poster 5</li>
                            <li>Poster 6</li>
                            <li>Poster 7</li>
                            <li>Poster 8</li>
                            <li>Poster 9</li>
                            <li>Poster 10</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add Links -->
<div class="modal fade" id="modalLinks" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?php echo form_open(base_url("manage/saveLinks")); ?>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="labelAddLinks"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php if($dataInformation["type"] == "tv"){ ?>
                        <div class="col-sm-2">
                            <?php
                                echo form_dropdown("cmbSeasons", $cmbSeasons, array(), "class='form-control' id='cmbSeason'");
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="cmbChapter" id="cmbChapter">
                                
                            </select>
                        </div>
                    <?phP }?>
                    <div class="col-sm-2">
                    <?php echo form_dropdown('cmbQuality', $cmbQuality, array(), "class='form-control' id ='cmbQuality'");?>
                    </div>
                    <div class="col-sm-2">
                    <?php echo form_dropdown('cmbLanguage', $cmbLanguage, array(), "class='form-control' id ='cmbLanguage'");?>
                    </div>
                    <div class="col-sm-2">
                    <?php echo form_dropdown('cmbSubtitle', $cmbSubtitle, array(), "class='form-control' id ='cmbSubtitle'");?>
                    </div>
                </div>
                <hr>
                
                <button type="button" class="btn btn-default btn-xs add" id="add">
                    <i class="fa fa-plus"></i>
                </button>
                <br><br>

                <div id="buildyourform"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                
                <button type="submit" value="<?php echo $dataInformation["id"] ?>" name="btnConfirm" id="btnConfirm" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>

<script>
    $(document).ready(function() {
        $("#add").click(function() {
            var lastField = $("#buildyourform div:last");
            console.log(lastField && lastField.length && lastField.data("idx"));
            var intId =
                (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $('<div class="row" id="field' + intId + '"/>');
            fieldWrapper.data("idx", intId);
            var fName = $(
                '<div class="col-sm-7"><input type="text" name="links[]" class="form-control fieldname" /></div>'
            );
            var fType = $(
                '<div class="col-sm-3"><?php echo $cmbServer ?></div>'
            );
            var removeButton = $(
                '<div class="col-sm-2"><button type="button" class="btn btn-danger btn-sm remove"><i class="fa fa-minus-circle"></i></button></div>'
            );
            
            removeButton.click(function() {
                $(this)
                    .parent()
                    .remove();
            });

            fieldWrapper.append(fType);
            fieldWrapper.append(fName);
            fieldWrapper.append(removeButton);
            fieldWrapper.append($("<br><br>"));

            $("#buildyourform").append(fieldWrapper);
	    });
    });
</script>

    <?php echo form_close(); ?>
</div>