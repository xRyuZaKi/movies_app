<div id="slidey" style="display:none;">
    <ul>
        <?php foreach($movies as $value){
            if(!isset($value["original_title"]) && !isset($value["title"])){
                $title = $value["original_name"];
            }else{
                $title = $value["title"];
            }

            ?>
            <li>
                <img src="<?php echo base_url("assets/images/poster/" . $value["backdrop_path"]) ?>">
                    <p class='title'>
                        <?php echo $title; ?>
                    </p>
                    <p class='description'> 
                        <?php echo $value["overview"] ?>
                    </p>
            </li>
        <?php } ?>
    </ul>   	
</div>