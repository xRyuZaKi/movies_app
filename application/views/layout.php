<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>One Movies an Entertainment Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //include CSS -->
<?php includeCSSResources(); ?>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>

<!-- //include JS -->
<?php includeJSResources(); ?>

</head>
	
<body>

	<!-- header -->
	<div class="header">
		<div class="container">
			<div class="w3layouts_logo">
				<a href="<?php echo base_url(); ?>"><h1>One<span>Movies</span></h1></a>
			</div>
			<div class="w3_search">
				<input type="text" class="search" id="search" name="Search" placeholder="Search" required="">
			</div>
			<div class="menu-profiler">
				<ul>
					<?php if(!GetFromSession("isloggedin")){?>
						<a class="btn btn-default" href="#" data-toggle="modal" data-target="#myModal">Iniciar Sesion</a>
					<?php }else{?>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<?php echo GetFromSession("name"); ?> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url("user/account.html") ?>"><i class="fa fa-user"></i> Mi Cuenta</a></li>
								<li><a href="<?php echo base_url("panel.html") ?>"><i class="fa fa-upload"></i> Panel Uploader</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="<?php echo base_url("user/logout.html") ?>"><i class="fa fa-sign-out"></i> Salir</a></li>
							</ul>
						</div>
					<?php } ?>
					
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->

<?php renderSection("modal-session"); ?>

<!-- nav -->
	<div class="movies_nav">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav>
						<ul class="nav navbar-nav">
							<li class="active"><a href="<?php echo base_url(); ?>">Inicio</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Generos <b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns-3">
									<li>
										<?php
											$genders = getAllGenders();
											foreach ($genders as $row){
											?>  
													<div class="col-sm-4">
														<ul class="multi-column-dropdown">
															<li><a href="genres.html"><?php echo mb_strtoupper($row["name"]); ?></a></li>
														</ul>
													</div>
											<?php
											}
										?>

										<div class="clearfix"></div>
									</li>
								</ul>
							</li>
							<li><a href="<?php echo base_url() ?>series.html">Tv-Series</a></li>
							<li><a href="<?php echo base_url() ?>movies.html">Peliculas</a></li>
						</ul>
					</nav>
				</div>
			</nav>	
		</div>
	</div>
<!-- //nav -->
<!-- banner -->
	<?php renderSection($section); ?>
	   
	<div class="general_social_icons">
		<nav class="social">
			<ul>
				<li class="w3_twitter"><a href="#">Twitter <i class="fa fa-twitter"></i></a></li>
				<li class="w3_facebook"><a href="#">Facebook <i class="fa fa-facebook"></i></a></li>
				<li class="w3_dribbble"><a href="#">Dribbble <i class="fa fa-dribbble"></i></a></li>
				<li class="w3_g_plus"><a href="#">Google+ <i class="fa fa-google-plus"></i></a></li>				  
			</ul>
		</nav>
	</div>
	
	<br>
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="w3ls_footer_grid">
				<div class="col-md-6 w3ls_footer_grid_left">
					<div class="w3ls_footer_grid_left1">
						<h2>Subscribe to us</h2>
						<div class="w3ls_footer_grid_left1_pos">
							<form action="#" method="post">
								<input type="email" name="email" placeholder="Your email..." required="">
								<input type="submit" value="Send">
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6 w3ls_footer_grid_right">
					<a href="index.html"><h2>One<span>Movies</span></h2></a>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-5 w3ls_footer_grid1_left">
				<p>&copy; 2016 One Movies. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
			<div class="col-md-7 w3ls_footer_grid1_right">
				<ul>
					<li>
						<a href="genres.html">Movies</a>
					</li>
					<li>
						<a href="faq.html">FAQ</a>
					</li>
					<li>
						<a href="horror.html">Action</a>
					</li>
					<li>
						<a href="genres.html">Adventure</a>
					</li>
					<li>
						<a href="comedy.html">Comedy</a>
					</li>
					<li>
						<a href="icons.html">Icons</a>
					</li>
					<li>
						<a href="contact.html">Contact Us</a>
					</li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</body>
<script>
	var baseUrl = function (){
		return "<?php echo base_url(); ?>"
	}
</script>
</html>