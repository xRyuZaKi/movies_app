<div class="single-page-agile-main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Detalles Descarga</h3>
                <hr>
                <div class="tabbable">
                    <ul class="nav nav-pills nav-stacked col-md-3">
                        <li class="active"><a href="#tab-quality" data-toggle="tab">Calidad</a></li>
                        <li><a href="#tab-language" data-toggle="tab">Idioma</a></li>
                        <li><a href="#tab-subtitle" data-toggle="tab">SubTitulos</a></li>
                        <li><a href="#tab-servers" data-toggle="tab">Servidores Descarga</a></li>
                    </ul>
                    <div class="tab-content col-md-9">
                        <div class="tab-pane active" id="tab-quality">
                        <?php renderSection("detaildownload/qualityView"); ?>
                        </div>
                        <div class="tab-pane" id="tab-language">
                        <?php renderSection("detaildownload/languageView"); ?>
                        </div>
                        <div class="tab-pane" id="tab-subtitle">
                        <?php renderSection("detaildownload/subtitleView"); ?>
                        </div>
                        <div class="tab-pane" id="tab-servers">
                        <?php renderSection("detaildownload/serverView"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>