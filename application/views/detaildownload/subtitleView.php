<aside>
    <button class="btn btn-default"
    data-toggle="modal" data-target="#modal-subtitle"
    >
        Agregar Subtitulos
    </button>
</aside>
<hr>
<table class="table table-condensed">
    <thead>
        <tr>
            <th>#</th>
            <th>Descripcion</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i = 0;
        foreach($subtitles as $item){
        ?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td><?php echo $item["description"]; ?></td>
                <td></td>
            </tr>
        <?php }?>    
    </tbody>
</table>

<?php print ModalAddDetailDownload("Agregar Subtitulos", "Subtitulo", "Detail_Download/save", "subtitle", "[Subtitulos: Ejem. Ingles, Español Latino...]"); ?>