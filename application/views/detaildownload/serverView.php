<aside>
    <button class="btn btn-default"
    data-toggle="modal" data-target="#modal-server"
    >
        Agregar Servidores
    </button>
</aside>
<hr>
<table class="table table-condensed">
    <thead>
        <tr>
            <th>#</th>
            <th>Descripcion</th>
            <th>Tipo</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        foreach($servers as $item){?>     
            <tr>
                <td><?php echo ++$i; ?></td>
                <td><?php echo $item["description"]; ?></td>
                <td><?php echo ucfirst($item["type"]); ?></td>
                <td></td>
            </tr>
        <?php }
        ?>
    </tbody>
</table>

<?php print ModalAddDetailDownloadServer(); ?>
