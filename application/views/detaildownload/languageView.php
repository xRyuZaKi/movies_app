<aside>
    <button class="btn btn-default"
    data-toggle="modal" data-target="#modal-language"
    >
        Agregar Idiomas
    </button>
</aside>
<hr>
<table class="table table-condensed">
    <thead>
        <tr>
            <th>#</th>
            <th>Descripcion</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i = 0;
        foreach($languages as $item){?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td><?php echo $item["description"] ?></td>
                <td></td>
            </tr>
        <?php }
        ?>
    </tbody>
</table>

<?php print ModalAddDetailDownload("Agregar Idiomas", "Idioma", "Detail_Download/save", "language", "[Idioma: Ejem. Ingles, Español Latino...]"); ?>