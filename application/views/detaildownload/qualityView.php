<aside>
    <button class="btn btn-default"
    data-toggle="modal" data-target="#modal-quality"
    >
        Agregar Calidad Descarga
    </button>
</aside>
<hr>
<table class="table table-condensed">
    <thead>
        <tr>
            <th>#</th>
            <th>Descripcion</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i = 0;
        foreach($qualities as $item){
        ?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td><?php echo $item["description"]; ?></td>
                <td></td>
            </tr>
        <?php }?>
    </tbody>
</table>

<?php print ModalAddDetailDownload("Agregar Calidad de Descarga", "Calidad", "Detail_Download/save", "quality", "[Calidad: Ejem. HD, BD-RIP...]"); ?>