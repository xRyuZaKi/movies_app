<div class="single-page-agile-main">
    <h4 class="latest-text w3_latest_text">PELICULAS</h4>
    <div class="container">
        <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
            <div class="w3_agile_featured_movies">
                <?php foreach($movies as $movie){ ?>
                <div class="col-md-2 w3l-movie-gride-agile">
                    <a href="<?php echo base_url("mostrar/pelicula-tv-show-".$movie["slug_url"]) ?>" class="hvr-shutter-out-horizontal">
                        <img style="height: 250px" src="<?php echo base_url("assets/images/poster/".$movie["poster_path"])?>" title="album-name" class="img-responsive" alt=" " />
                        <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
                    </a>
                    <div class="mid-1 agileits_w3layouts_mid_1_home">
                        <div class="w3l-movie-text">
                            <h6><a href="single.html"><?php echo $movie["original_title"] ?></a></h6>							
                        </div>
                        <div class="mid-2 agile_mid_2_home">
                            <p><?php echo date('Y',strtotime($movie['release_date'])); ?></p>
                            <div class="block-stars">
                                <i class="fa fa-star" aria-hidden="true"></i> <?php echo $movie["vote_average"] ?>/10
                            </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>