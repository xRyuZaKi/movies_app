<?php

class Main_Model extends CI_Model{

    public function getAllGenders(){
        $query = $this->db->get("gender");

        return $query->result_array();
    }

    public function insertGender($dataInsert){
        $sql = "INSERT IGNORE INTO gender (cod_gender, name) VALUES (?, ?)";

        $this->db->query($sql, [$dataInsert["id"], $dataInsert["name"]]);
    }

    
}