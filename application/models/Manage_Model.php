<?php
class Manage_Model extends CI_Model{

    public function save($dataInsert, $genres, $actors, $seasonChapters){
        $this->db->trans_begin();
        
        $this->db->insert("movie_tv_show", $dataInsert);

        $idMovieTvShow = $this->db->insert_id();

        foreach($genres as $value){
            $genreMovieTvShow = [
                "id_movie_tv" => $idMovieTvShow,
                "cod_genre" => $value
            ];
            $this->db->insert("genres_movie_tv", $genreMovieTvShow);
        }

        foreach($actors as &$item){
            $item["id_movie_tv"] = $idMovieTvShow;

            $this->db->insert("movie_tv_actors", $item);
        }

        if(!empty($seasonChapters)){
            $this->saveSeasonChapters($idMovieTvShow, $seasonChapters);
        }
        if($this->db->trans_status() == false){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }
    }

    public function genresMovieTv($idMovieTv){
        $sql = "SELECT
        genre.*
    FROM
        genres_movie_tv genreMovie
    INNER JOIN movie_tv_show movieTv ON genreMovie.id_movie_tv = movieTv.id
    INNER JOIN gender genre ON genre.cod_gender = genreMovie.cod_genre
    where genreMovie.id_movie_tv = ?";
        $query = $this->db->query($sql, array($idMovieTv));

        return $query->result_array();
    }

    public function actorsMovieTv($idMovieTv){
        $sql = "select * from movie_tv_actors where id_movie_tv = ?";
        $query = $this->db->query($sql, array($idMovieTv));

        return $query->result_array();
    }

    public function getAllMovieTvShow(){
        $query = $this->db->get("movie_tv_show");

        return $query->result_array();
    }

    public function isValidSlugUrl($slugUrl){
        $this->db->where("slug_url", $slugUrl);
        $query = $this->db->get("movie_tv_show");

        return $query->row_array();
    }

    public function saveLinkDownloadOnline(){
        $this->db->trans_begin();

        $dataPost = (array)GetAllPost();

        $idMovieTv = $dataPost["btnConfirm"];

        $dataInsertDetail = [
            "id_quality" => $dataPost["cmbQuality"]
            , "id_subtitle" => $dataPost["cmbSubtitle"]
            , "id_language" => $dataPost["cmbLanguage"]
            , "id_movie_tv" => $idMovieTv
        ];

        $this->db->insert("detail_download", $dataInsertDetail);
        $idDetailDownload = $this->db->insert_id();

        /* detail donwload for tvShow */
        if(isset($dataPost["cmbSeasons"]) && $dataPost["cmbSeasons"] != "defualt"){
            $dataInsertDetailTvShow = [
                "id_season" => $dataPost["cmbSeasons"],
                "id_chapter" => $dataPost["cmbChapter"],
                "id_detail_download" => $idDetailDownload
            ];

            $this->db->insert("detail_download_tv_show", $dataInsertDetailTvShow);
        }

        $servers = $dataPost["cmbServer"];
        $links = $dataPost["links"];

        foreach($servers as $key => $value){
            $dataInsertLink = [
                "id_server" => $value
                , "link" => $links[$key]
                , "id_detail_download" => $idDetailDownload
            ];

            $this->db->insert("detail_download_links", $dataInsertLink);
        }

        if($this->db->trans_status() == false){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }
    }

    public function saveDetailDownload($table, $dataInsert){
        return $this->db->insert($table, $dataInsert);
    }

    public function getDetailDownload($table){
        $this->db->where("active", TRUE);
        $query = $this->db->get($table);

        return $query->result_array();
    }

    public function getMovieTv($idMovieTv){
        $this->db->where("id", $idMovieTv);
        $query = $this->db->get("movie_tv_show");

        return $query->row_array();
    }

    public function saveSeasonChapters($idMovieTv, $details){
        $this->db->trans_begin();

        foreach($details as $item){
            $episodes = $item["episodes"];
            unset($item["episodes"]);

            $item["id_movie_tv"] = $idMovieTv;
            $this->db->insert("tv_seasons", $item);
            $idTvSeason = $this->db->insert_id();

            foreach($episodes as $key => &$value){
                $episodes[$key]["id_tv_season"] = $idTvSeason;
            }

            $this->db->insert_batch("tv_episodes", $episodes);
        }

        if($this->db->trans_status() == false){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
        }
    }

    public function getLinks($idMovieTv = null){
        $fields = "  
            serv.description as server
            , serv.type as typeDownload
            , lang.description as language
            , sub.description as subtitle
            , qua.description as quality
            , links.link as linkDownload";

        ($idMovieTv == null) ? $fields = "movieTv.*" : NULL ;

        $sql = "select $fields
        from detail_download detail
        inner join detail_download_links links on detail.id = links.id_detail_download
        inner join subtitle sub on sub.id = detail.id_subtitle and sub.active = 1
        inner join quality qua on qua.id = detail.id_quality and qua.active = 1
        inner join language lang on lang.id = detail.id_language and lang.active = 1
        inner join server serv on serv.id = links.id_server and serv.active = 1
        inner join movie_tv_show movieTv on movieTv.id = detail.id_movie_tv
        ";

        ($idMovieTv != NULL)? $sql.= " where detail.id_movie_tv = ?" : NULL;

        if($idMovieTv != NULL){
            $query = $this->db->query($sql, array($idMovieTv));
        }else{
            $sql.=" group by detail.id_movie_tv";
            $query = $this->db->query($sql);
        }

        return $query->result_array();
    }

    public function getSeasons($idTv){
        $sql = "SELECT
                seasons.*
            FROM
                movie_tv_show tvShow
            INNER JOIN tv_seasons seasons ON tvShow.id = seasons.id_movie_tv
            INNER JOIN tv_episodes episodes ON seasons.id = episodes.id_tv_season
            where tvShow.id = ?
            group by seasons.id";
        $query = $this->db->query($sql, [$idTv]);

        return $query->result_array();
    }

    public function getChapters($idTv, $idSeason){
        $sql = "SELECT
                episodes.*
            FROM
                movie_tv_show tvShow
            INNER JOIN tv_seasons seasons ON tvShow.id = seasons.id_movie_tv
            INNER JOIN tv_episodes episodes ON seasons.id = episodes.id_tv_season
            where tvShow.id = ? and seasons.season_number = ?";
        $query = $this->db->query($sql, [$idTv, $idSeason]);

        return $query->result_array();
    }

    public function getLinksTVShow($idTvShow){
        $sql = "SELECT
        episodes.*
        , seasons.season_number
        ,serv.description as server
        , serv.type as typeDownload
        , lang.description as language
        , sub.description as subtitle
        , qua.description as quality
        , detailLinks.link as linkDownload
    FROM
        movie_tv_show tvShow
    INNER JOIN tv_seasons seasons ON tvShow.id = seasons.id_movie_tv
    INNER JOIN tv_episodes episodes ON seasons.id = episodes.id_tv_season
    inner join detail_download detail on detail.id_movie_tv = tvShow.id
    inner join detail_download_tv_show detailTV on detail.id = detailTV.id_detail_download and seasons.season_number = detailTV.id_season and episodes.id = detailTV.id_chapter
    inner join detail_download_links detailLinks on detailLinks.id_detail_download = detail.id
    
    inner join subtitle sub on sub.id = detail.id_subtitle and sub.active = 1
    inner join quality qua on qua.id = detail.id_quality and qua.active = 1
    inner join language lang on lang.id = detail.id_language and lang.active = 1
    inner join server serv on serv.id = detailLinks.id_server and serv.active = 1
    WHERE
        tvShow.id = ?";

        $query = $this->db->query($sql, [$idTvShow]);
        return $query->result_array();
    }
}