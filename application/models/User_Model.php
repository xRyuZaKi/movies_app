<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model{

    public function _validateCredentials($credentials){
        $username = $credentials["username"];
        $password = $credentials["password"];

        $this->db->where("login", $username);
        $this->db->where("password", $password);

        $query = $this->db->get("user");

        if($query->num_rows() > 0){
            return $query->row_array();
        }

        return FALSE;
    }
}