<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * SuperController that extends from CI_Controller in order to
 * create static methods to override the load->view existing method 
 * by self::MakeView() and sending $settings for Main Template and more.
 * Is named MZ_Controller.
 * 
 * All the custom controllers are going
 * to extends from this MZ_Controller now.
 * 
 * Created By Alex Acosta 2014
 * @xcytek
 **/
class MZ_Controller extends CI_Controller
{
    /** Default Template as Constant **/
	const DEFAULT_LAYOUT = 'layout';

	/** @var MZ_Controller $instance */
	private static $instance;
    
    /** Custom Layout **/
	private static $layout = '';
	
	/** Have the Model Instance **/
	protected static $model;
	
	/** Have the Library Name Instance **/
	protected static $lib;
        
	/** Array Rules of Validation Form **/
	protected static $rules= array();

	protected static $secretKey;
	protected static $urlAPI;
    
    /** Call Parent Construct from CI_Controller **/
	public function __construct(){
		parent::__construct();
		self::$layout = self::DEFAULT_LAYOUT;

		$this->config->load("api");

		self::$secretKey = $this->getSecretKey();
		self::$urlAPI = $this->getUrlAPI();
	}
	
	/**
	 * This method replace the normal $this->load->view
	 * in order to have a clear method to call instead
	 **/
	protected function MakeView($settings = null){				
		$this->load->view(self::$layout, $settings);
	}

	protected function LoadLibrary($name){
	    $this->load->library($name);
	    self::$lib = $this->$name;
	}

	protected function LoadModel($name){
	    $this->load->model($name);
	    self::$model = $this->$name;

		return self::$model;
	}

	protected function getSecretKey(){
		return $this->config->item("secret");
	}

	protected function getUrlAPI(){
		return $this->config->item("urlAPI");
	}

	protected function UserAuth($url = ""){
		if (!GetFromSession('isloggedin') && $url === '') {
			redirect(base_url());
		} elseif (GetFromSession('isloggedin') && $url !== '') {
		    redirect(base_url() . $url);
		}

		return true;
	}
	
}