/*
Navicat MySQL Data Transfer

Source Server         : Desarrollo
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : movies_app

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2018-06-03 20:41:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for detail_download
-- ----------------------------
DROP TABLE IF EXISTS `detail_download`;
CREATE TABLE `detail_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_quality` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `id_subtitle` int(11) NOT NULL,
  `id_movie_tv` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for detail_download_links
-- ----------------------------
DROP TABLE IF EXISTS `detail_download_links`;
CREATE TABLE `detail_download_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_server` int(11) NOT NULL,
  `link` text,
  `id_detail_download` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for detail_download_tv_show
-- ----------------------------
DROP TABLE IF EXISTS `detail_download_tv_show`;
CREATE TABLE `detail_download_tv_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_season` int(11) NOT NULL,
  `id_chapter` int(11) NOT NULL,
  `id_detail_download` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for gender
-- ----------------------------
DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_gender` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx__cod_gender__name` (`cod_gender`,`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for genres_movie_tv
-- ----------------------------
DROP TABLE IF EXISTS `genres_movie_tv`;
CREATE TABLE `genres_movie_tv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_movie_tv` int(11) NOT NULL,
  `cod_genre` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_movie_tv` (`id_movie_tv`),
  KEY `genres_movie_tv_ibfk_2` (`cod_genre`),
  CONSTRAINT `genres_movie_tv_ibfk_1` FOREIGN KEY (`id_movie_tv`) REFERENCES `movie_tv_show` (`id`),
  CONSTRAINT `genres_movie_tv_ibfk_2` FOREIGN KEY (`cod_genre`) REFERENCES `gender` (`cod_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for movie_tv_actors
-- ----------------------------
DROP TABLE IF EXISTS `movie_tv_actors`;
CREATE TABLE `movie_tv_actors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_movie_tv` int(11) NOT NULL,
  `name` text NOT NULL,
  `character` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_movie_tv` (`id_movie_tv`),
  CONSTRAINT `movie_tv_actors_ibfk_1` FOREIGN KEY (`id_movie_tv`) REFERENCES `movie_tv_show` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for movie_tv_show
-- ----------------------------
DROP TABLE IF EXISTS `movie_tv_show`;
CREATE TABLE `movie_tv_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backdrop_path` text,
  `poster_path` text,
  `id_tmdb` int(11) NOT NULL,
  `original_title` text,
  `original_name` text,
  `title` text,
  `overview` text,
  `release_date` date DEFAULT NULL,
  `first_air_date` date DEFAULT NULL,
  `vote_average` decimal(10,2) DEFAULT NULL,
  `runtime` int(11) DEFAULT NULL,
  `type` enum('movie','tv') DEFAULT NULL,
  `slug_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for quality
-- ----------------------------
DROP TABLE IF EXISTS `quality`;
CREATE TABLE `quality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for server
-- ----------------------------
DROP TABLE IF EXISTS `server`;
CREATE TABLE `server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `type` enum('online','download') NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for subtitle
-- ----------------------------
DROP TABLE IF EXISTS `subtitle`;
CREATE TABLE `subtitle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tv_episodes
-- ----------------------------
DROP TABLE IF EXISTS `tv_episodes`;
CREATE TABLE `tv_episodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `episode_number` int(255) NOT NULL,
  `name` text NOT NULL,
  `overview` text,
  `air_date` date DEFAULT NULL,
  `id_tv_season` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tv_season` (`id_tv_season`),
  CONSTRAINT `tv_episodes_ibfk_1` FOREIGN KEY (`id_tv_season`) REFERENCES `tv_seasons` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tv_seasons
-- ----------------------------
DROP TABLE IF EXISTS `tv_seasons`;
CREATE TABLE `tv_seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season_number` int(11) NOT NULL,
  `name` text NOT NULL,
  `overview` text,
  `air_date` date DEFAULT NULL,
  `id_movie_tv` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_movie_tv` (`id_movie_tv`),
  CONSTRAINT `tv_seasons_ibfk_1` FOREIGN KEY (`id_movie_tv`) REFERENCES `movie_tv_show` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text,
  `password` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
